#This a simulation of the triangular Ising model in which each
#spin site has 6 nearest neighbours making it more energetically favorable to the
#square lattice
import numpy as np
import math
import matplotlib.pyplot as plt
import random
#Initialising Values
N = 50  #Lattice Dimension (NxN)
k = 1.   #Boltzmann Constant
J = 1.   #Spin interaction constant


#function to create initial randomised lattice of 1's and -1's
def initial_lattice(N):

    lat = np.zeros((N,N), int)
    for i in range (N):
            for j in range (N):
                lat[i][j]= np.random.choice([-1,1])
    return lat

#once this function has been used to create initial random lattice it has no more need to be used elsewhere
latt = initial_lattice(N)
#plt.imshow(latt)
#function to define boundar conditions of lattice (spins on right edge of row interact with spins on left edge)
def boundary(i):
    if i > N - 1:
           return 0
    if i < 0:
           return N - 1
    else:
           return i
#function to calculate the Hamiltonian of a spin site, based on spin-spin interactions between neighbouring spins.
#each spin has six nearest neighbours
def Ham_Spin(i,j):
    return (-1 * latt[i,j]) * (latt[boundary(i + 1), j] +
                                            latt[boundary(i - 1 ), j] +
                                            latt[boundary(i-1), boundary(j+1)]+
                                            latt[boundary(i+1), boundary(j - 1)]+
                                            latt[i, boundary(j + 1)] +
                                            latt[i, boundary(j - 1)])
#function to sum the Hamiltonians of each spin site to calclulate the energy of the entire lattice
def Energy(latt):
    energy = 0      #energy is zero before first loop
    for j in range(N):
        for i in range(N):
            energy += Ham_Spin(i,j)/ 6.   #dividing to ensure no double counting
    return energy  #(a number that is the sum of the hamiltonian of each spin site)
#function to calculate the total magnetization (spin excess) of the lattice


#Beginning of the Metropolis Algorithm
N_arr  = list(range(0,N))  #a list of N values equal to the dimension of the latice

n = 300     #number of counts in the Metropolis loop
relax_t = 50  #relaxation time, number of sweeps carried out before values are collected
#Initialising lists to be used in the algorithm
Elist = []
e_List = []
T = 1.0   #initial temperature, before algorithm
Tlist = []  #list of temperatures
#fig = plt.figure() #can be used to view evolution of the lattice
while(T < 6):  #collecting values up as far as T = 6

        for count in range(n):
        # choose random spin site
            for i in range(1, N-1):
                for j in range(1, N-1):

                    dE = 2 * Ham_Spin(i, j) * J        #energy expended when spin is flipped

                    if (dE >= 0) or (dE < 0 and math.exp(dE/(k*T))>= random.random()):
                        latt[i,j] = -1*latt[i,j]    #flips the spin as this energy is favourable
                    #otherwise the spin remains the same and the process is repeated
                    else:
                        latt[i,j] = latt[i,j]
        #the following lines can be used to view evolution of the lattice, but greatly slow down run time
        #plt.imshow(latt)
        #fig.canvas.draw()
        #plt.pause(0.3)
            #initialising values
            en = 0


            #values are appended after relaxation time
            if count > relax_t:
                en += Energy(latt)/2500. #energy per spin
                #appending lists with above values
                Elist.append(en)    #list of final energies of lattice


        e_vals =min(Elist)
        e_List.append(e_vals)
        Elist = []
        Tlist.append(T)
        T += 0.1 #increasing temperature by 0.1 each iteration


plt.plot(Tlist, e_List, 'o')
plt.title(' Triangular Lattice: Average Energy per Spin')
plt.errorbar(Tlist, e_List, xerr = 0, yerr = 0)
plt.xlabel("Temperature ($J/K_B)$")
plt.ylabel('E')

plt.show()
