Using the Metropolis algorithm, the 2-dimensional Ising model was simulated with no external magnetic field
and analysed to identify whether it undergoes a second-order phase transition, and if so, to determine at 
what temperature this occurs. 