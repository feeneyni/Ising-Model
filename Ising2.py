#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 11 20:39:57 2018

@author: NiamhFeeney

This is the main body of code which models the Ising model lattice, gathers data
from it, then plots the data
"""


import numpy as np
import math
import matplotlib.pyplot as plt
import random
#Initialising Values
N = 50  #Lattice Dimension (NxN)
k = 1.   #Boltzmann Constant
J = 1.   #Spin interaction constant


#function to create initial randomised lattice of 1's and -1's
def initial_lattice(N):

    lat = np.zeros((N,N), int)
    for i in range (N):
            for j in range (N):
                lat[i][j]= np.random.choice([-1,1])
    return lat

#once this function has been used to create initial random lattice it has no more need to be used elsewhere
latt = initial_lattice(N)
#function to define boundar conditions of lattice (spins on right edge of row interact with spins on left edge)
def boundary(i):
    if i > N - 1:
           return 0
    if i < 0:
           return N - 1
    else:
           return i
#function to calculate the Hamiltonian of a spin site, based on spin-spin interactions between neighbouring spins.
#each spin has four neighbours
def Ham_Spin(i,j):
    return (-1.0 * latt[i,j])*(latt[boundary(i + 1), j] +
                                            latt[boundary(i - 1 ), j] +
                                            latt[i, boundary(j + 1)] +
                                            latt[i, boundary(j - 1)])
#function to sum the Hamiltonians of each spin site to calclulate the energy of the entire lattice
def Energy(latt):
    energy = 0      #energy is zero before first loop
    for j in range(N):
        for i in range(N):
            energy += Ham_Spin(i,j)/4.    #dividing to ensure no double counting
    return energy  #(a number that is the sum of the hamiltonian of each spin site)
#function to calculate the total magnetization (spin excess) of the lattice
def Mag(latt):
    mag = 0     #initial magnetization zero
    for j in range(N):
            for i in range(N):
                     mag +=latt[i,j]    #adds the spin values of each spin site
    return mag

#Beginning of the Metropolis Algorithm
N_arr  = list(range(0,N))  #a list of N values equal to the dimension of the latice

n = 300     #number of counts in the Metropolis loop
relax_t = 50  #relaxation time, number of sweeps carried out before values are collected
#Initialising lists to be used in the algorithm
Elist = []
Mlist = []
e_List = []
m_List = []
cap_list = []
caplist = []
sus_list = []
suslist = []
T = 0.1   #initial temperature, before algorithm
Tlist = []  #list of temperatures
#fig = plt.figure() #can be used to view evolution of the lattice
while(T < 6):  #collecting values up as far as T = 6

        for count in range(n):
        # choose random spin site
            for i in range(N):
                for j in range(N):

                    dE = 2 * Ham_Spin(i, j) * J        #energy expended when spin is flipped

                    if (dE >= 0) or (dE < 0 and math.exp(dE/(k*T))>= random.random()):
                        latt[i,j] = -1*latt[i,j]    #flips the spin as this energy is favourable
                    #otherwise the spin remains the same and the process is repeated
                    else:
                        latt[i,j] = latt[i,j]
        #the following lines can be used to view evolution of the lattice, but greatly slow down run time
        #plt.imshow(latt)
        #fig.canvas.draw()
        #plt.pause(0.3)
            #initialising values
            en = 0
            M = 0
            en_sq = 0
            M_sq = 0
            cap = 0
            sus = 0

            #values are appended after relaxation time
            if count > relax_t:
                en += Energy(latt)/2500. #energy per spin
                en_sq += (en)**2    #energy per spin squared
                cap += (1./N*k*(T)**2)*((en_sq)/250. - (en*en)/62500.) #heat capacity

                M += abs(Mag(latt)) / 2500. #magnetization per spin
                M_sq += (M)**2 #magnetization per spin squared
                sus += (1./N*k*T)*((M_sq)/250. - (M*M)/62500.) #magnetic susceptibility

                #appending lists with above values
                Mlist.append(M)
                Elist.append(en)    #list of final energies of lattice
                caplist.append(cap)
                suslist.append(sus)
#can be used to view final lattice at each temperature
#plt.imshow(latt)
#plt.colorbar()
#plt.title('Ising Model')
#plt.show()


        e_vals = min(Elist) #average of energy values
        m_vals = Mlist[n - relax_t - 2]#average of mag values

        cap_vals = caplist[n - relax_t - 2]   #average of heat capacity
        sus_vals= suslist[n - relax_t - 2]  #average of magnetic susceptibility

        #collecting lists of these values from each temperature
        e_List.append(e_vals)
        m_List.append(m_vals)
        cap_list.append(cap_vals)
        sus_list.append(sus_vals)
        #clearing these lists for next iteration of te temperture loop
        Mlist = []
        Elist = []
        caplist = []
        suslist = []
        #appending list of temperature values
        Tlist.append(T)
        T += 0.1 #increasing temperature by 0.1 each iteration

        #plt.imshow(latt)
        #plt.colorbar()
        #plt.title('Ising Model')
        #plt.show()
#plotting all the collected values
plt.plot(Tlist, e_List, 'o')
plt.title('Average Energy per Spin')
plt.errorbar(Tlist, e_List, xerr = 0, yerr = 0)
plt.xlabel("Temperature ($J/K_B)$")
plt.ylabel('E')
plt.plot(Tlist, m_List, 'o', markersize = 3)
plt.errorbar(Tlist,m_List,xerr = 0.0, yerr = 0)
plt.title('Magnetization')
plt.ylabel('Average Lattice Magnetization')
plt.plot(Tlist, cap_list, 'o', markersize = 3)
plt.title('Heat Capacity')
plt.ylabel('Specific Heat Capacity $(J/k^2)$')
plt.errorbar(Tlist, cap_list, xerr = 0, yerr = 0)
plt.plot(Tlist, sus_list, 'o', markersize = 3)
plt.errorbar(Tlist, sus_list, xerr = 0, yerr = 0)
plt.xlabel("Temperature ($J/K_B)$")
plt.ylabel('$\chi$')
plt.title('Magnetic Susceptibility')
plt.show()
